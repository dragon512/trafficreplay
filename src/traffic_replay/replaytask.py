'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
'''


import sys
import ssl
import aiohttp
import yarl
import socket
import asyncio

from traceback import print_exc


from trlib import Transaction, Session, Request, Response
from .connectioninfo import ConnectionInfo

# this file defines the logic to send all the data to


async def replay_session(session: Session, connection_data:ConnectionInfo, verify_header: bool = False, verify_body: bool = False, proxy_request=False, proxy_response=False):
    #return await asyncio.sleep(.5)
    for jj in range(1,10):
        try:
            default_request = session.transactions[0].proxyRequest if proxy_request else session.transactions[0].clientRequest
            http_version = default_request.version

            if http_version == "1.0":
                pass
            elif http_version == "1.1":
                http_version = aiohttp.HttpVersion11
            elif http_version == "2.0":
                print("http 2.0 not implemented.. fallback to 1.1")
                http_version = aiohttp.HttpVersion11

            if default_request.scheme in ("https",):            
                ssl_context = connection_data.ssl_context
                proxy = "https://{}:{}".format(connection_data.host, connection_data.sslport)
            else:
                ssl_context = False
                proxy = "http://{}:{}".format(connection_data.host, connection_data.port)

            family = socket.AF_INET6 if session.protocol == "ipv6" else socket.AF_INET        

            conn = aiohttp.TCPConnector( family = family)
            

            async with aiohttp.ClientSession(connector= conn, skip_auto_headers=[], trust_env=False, version=http_version) as client:

                for transaction in session.transactions:
                    # the request and response for this transaction
                    request: Request = transaction.proxyRequest if proxy_request else transaction.clientRequest
                    response: Response = transaction.proxyResponse if proxy_response else transaction.serverResponse
                    try:
                        async with await client.request(
                                                    proxy=proxy,
                                                    method=request.method,
                                                    url=yarl.URL(request.url, encoded=True),
                                                    skip_auto_headers=request.headers.asHeaderDict.keys(),
                                                    headers=request.headers.asHeaderDict,
                                                    data=request.body.content,
                                                    ssl=ssl_context,
                                                    chunked = request.headers.isChunkedEncoded
                                                ) as response:
                            if verify_header:
                                # test headers and content value and return
                                pass
                            if verify_body:
                                pass
                            # we always test result code
                            # test that we have return code
                            if response.status != response.status:
                                print("Status code failed")
                                # status code mismatch
                                return False
                    except Exception as e:
                        print("Exception happened during session ",type(e),e)
                        return False
        except Exception as e:
            print("Exception happened",e)
            return False
    return True
