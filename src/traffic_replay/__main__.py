'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
'''

import argparse
import sys
import time
#import traffic_replay.mainProcess as main
from .engine import Engine
from trlib.taskpool import create_pool, Task, AsyncTask, TaskPool
import traffic_replay.glb as glb


def main() -> None:

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--mode", "-m",
        type=str,
        default="replay-no-delay",
        help="[replay replay-no-delay load-test]"
    )

    parser.add_argument(
        "--workers", "-w", "-j",
        type=int,
        help="number of workers to create to process requests"
    )

    parser.add_argument(
        "--data-dir", "--log-dir", "--log_dir", "-l",
        type=str,
        required=True,
        help="directory of JSON replay files"
    )

    parser.add_argument(
        "--verify-body",
        action="store_true",
        help="verify response status code"
    )

    parser.add_argument(
        "--verify-header",
        action="store_true",
        help="verify response status code"
    )

    parser.add_argument(
        "--use-proxy-request",
        action="store_true",
        help="send request based on transaction proxy-request instead of client-request"
    )

    parser.add_argument(
        "--use-server-response",
        action="store_true",
        help="verify response based on transaction server-response instead of proxy-response"
    )

    parser.add_argument(
        "--host", "-H",
        default="127.0.0.1",
        help="proxy/host IP to send the requests to (default: 127.0.0.1)"
    )

    parser.add_argument(
        "--port", "-p",
        type=int,
        default="None",
        help="The non secure port of ATS to send the request to"
    )

    parser.add_argument(
        "--ssl-port","--s-port","--s_port", "-s",
        type=int,
        default=None,
        help="The secure port of ATS to send the request to"
    )

    parser.add_argument(
        "--ca-cert", "--ca_cert", "-c",
        help="Certificate to present"
    )

    parser.add_argument(
        "--key", "-k",
        help="Key for ssl connection"
    )

    parser.add_argument(
        "--colorize",
        action="store_true",
        default=False,
        help="specify whether to colorize the output"
    )

    args = parser.parse_args()

    pool:TaskPool = create_pool(args.workers,shared_queue=False)
    
    engine = Engine(
        args.data_dir, # get yaml data here
        args.host, # send data to this host
        args.port, # the port to use
        args.ssl_port, # the port to use if https
        args.mode, # what logic to run
        args.verify_header,
        args.verify_body,
        args.use_proxy_request, # which set of data to use for request
        args.use_server_response, # which set of data to use for reponse
        #args.key, # SSL key
        pool)

    st = time.time()
    pool.run(engine.start)
    print("TT=",time.time() - st)
    

if __name__ == '__main__':
    main()
