'''
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
'''

import sys
import ssl
import aiohttp
import yarl

import asyncio
import glob
import os
import time
import socket
from typing import Any, List, Optional, Union, cast
from trlib import Transaction, Session, Request, Response
from .connectioninfo import ConnectionInfo
from trlib import loads, ParseError, Dom
import trlib.taskpool as taskpool
from .replaytask import replay_session
#import replay_h1


def load_file(filen):
    try:
        if os.path.isfile(filen):
            with open(filen) as infile:
                data = infile.read()
            try:
                #print("Loading {}".format(filen))
                dom = loads(data)
                #print("Done Loading {}, {}".format(filen, dom))
                if not dom.sessions:
                    print("Empty sessions in {}".format(filen))
                    os.remove(filen)
                else:
                    for cnt, session in enumerate(dom.sessions):
                        if not session.transactions:
                            print(
                                "Empty transactions in session[{}] file {}".format(cnt, filen))

                return dom
            except ParseError as e:
                print("Failed to load", filen, e)
        else:
            print("Error! {} was not found".format(filen))
    except:
        print("Exception while loading {}".format(filen))
        raise
    print("Returning NONE from Load File")
    return None


async def send_session(session, host: str, port: str, ssl: bool):
    asyncio.open_connection(host, port, ssl=ssl)


class Engine:
    def __init__(self, datadir: str, host: str, port: str, sslport: str, mode: str, verify_header: str, verify_body: str, use_proxy_request: bool, use_server_response: bool, pool: taskpool.TaskPool) -> None:
        self._hostname: str = host
        self._port: str = port
        self._sslport: str = sslport
        self._pool: taskpool.TaskPool = pool
        self._datadir: str = datadir
        self._dom: Optional[Dom] = None
        self._mode=mode
        self._verify_header=verify_header
        self._verify_body=verify_body
        self._proxy_request=use_proxy_request
        self._server_response=use_server_response

    async def start(self):
        '''
        We start up n processes and give each a queue. We pass Tasks and AyncTask to the queue
        to get processed by the workers.
        Tasks get run a a thread on the worker processes
        AyncTasks are run in the main thread of the worker processes event loop
        '''
        # process the data files
        await self._load_data()

        # send the data
        if self._mode == "replay":
            print("Not implemented")
            return 1
        elif self._mode == "replay-no-delay":
            return await self._replay_no_delay()
        elif self._mode == "load-test":
            return await self._replay_load()
        else:
            print("'{}' is not a valid mode".format(self._mode))
            return 1

        return

    async def _load_data(self):
        '''
        Read all .json data in the data directory
        '''
        # scan the directory for all data files
        # don't recurse
        tasks = taskpool.TaskSet()
        # load all the files
        print("Loading json data in", self._datadir)
        cnt = 0
        st = time.time()
        for item in glob.iglob(os.path.join(self._datadir, "*.json")):
            cnt += 1
            task = taskpool.Task(load_file, item)
            task.on_finished.connect(lambda : self.merge_dom(task.result()))
            tasks.add(task).add_to(self._pool)            
        # gather the results
        print("Waiting for files to finish loading")
        await tasks.wait_all()
        print("loaded {} file/sec".format(cnt/(time.time()-st)))
        print(self._dom)
        # result.sort()

    def merge_dom(self, dom: Dom) -> None:
        if not self._dom and dom:
            self._dom = dom
        elif dom and dom.sessions:
            self._dom.sessions.extend(dom.sessions)

    def verify_server_http(self) -> bool:
        '''
        Checks to see if the server is running on `hostname` and `port`
        '''
        if self._port:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((self._hostname, self._port))
            if result != 0:
                return False
        return True  # return true if there is no server to check for

    def verify_server_https(self) -> bool:
        '''
        Checks to see if the server is running on `hostname` and `sslport`
        If not running, this function will terminate the script
        '''
        if self._port:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((self._hostname, self._sslport))
            if result != 0:
                return False
        return True  # return true if there is no server to check for

    async def _replay_load(self) -> bool:
        # dump impl.. requires ctrl-c to stop
        tasks = taskpool.TaskSet()
        ctx = False #ssl.create_default_context()
        ci = ConnectionInfo(self._hostname, self._port, self._sslport, ctx)
        st = time.time()
        cnt = 0
        total_time = 10
        total_size=10000*2
        #while time.time() - st < total_time:
        import pickle
        
        for session in cast(Dom,self._dom).sessions:
            print("pickle size",len(pickle.dumps(taskpool.AsyncTask(replay_session, 1, None, None, None, None, None))))
            #print("pickle size",len(pickle.dumps(taskpool.AsyncTask(replay_session, session, ci, self._verify_header, self._verify_body, self._proxy_request, self._server_response))))
            #print("pickle size",len(pickle.dumps(taskpool.AsyncTask(replay_session, "a"*3691, ci, self._verify_header, self._verify_body, self._proxy_request, self._server_response))))
            #pd = cpickle.dumps(session)
            for i in range(0,total_size):
                # change to aynctask            
                tasks.add(
                    #taskpool.AsyncTask(replay_session, 3, None, None, None, None, None).add_to(self._pool)
                    taskpool.AsyncTask(replay_session, session, ci, self._verify_header, self._verify_body, self._proxy_request, self._server_response).add_to(self._pool)
                    #taskpool.AsyncTask(replay_session, "a"*3691, ci, None, None, None, None, None, None).add_to(self._pool)
                )
                #cnt+=1
        #print("waiting for everything to finish")
        tt = time.time()-st
        print("load test {} pushes/sec {} sec".format(total_size/tt,tt))
        self._pool._stop()
        await tasks.wait_all()
        tt = time.time()-st
        print("{} sessions/sec {} sec".format((total_size/tt)*10,tt))


    async def _replay_no_delay(self) -> bool:
        '''
        In this case we want to run all the session once in order in which they happened
        This is close to the order as various system hickups could start one session before another
        however it should be close enough.
        '''
        # this case has a shared queue, so we just fill it up as fast as we can
        # given the queue is shared only on process can read it at a time
        # this should give us the basic order guarrentee we need
        tasks = []
        ctx = False #ssl.create_default_context()
        ci = ConnectionInfo(self._hostname, self._port, self._sslport, ctx)

        for session in cast(Dom,self._dom).sessions:
            # change to aynctask
            tasks.append(
                taskpool.AsyncTask(replay_session, session, ci, self._verify_header, self._verify_body, self._proxy_request, self._server_response).add_to(self._pool)
                )
            # dirty way to work around how many total sessions we allow on queue
            enter = True
            while len(tasks) > self._pool.size()*2 or enter:
                enter = False
                result, tasks = await taskpool.gather_done(tasks)
        print("waiting for everything to finish")
        await taskpool.gather(tasks)

        return False
    # async def _schedule_session(self,session) -> None:
        # task = taskpool.AsyncTask(func=send_session, session ,pool=self._pool)
        # taskpool.pool.push(task)
